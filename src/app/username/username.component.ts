import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-username',
  templateUrl: './username.component.html',
  styleUrls: ['./username.component.css']
})
export class UsernameComponent implements OnInit {

  userName = ''; //variable to hold userName value

  constructor() { }

  ngOnInit() {
  }

 //Function for empty the userName when button clicked
 emptyUserName() {  
         this.userName = " ";
  }

}
